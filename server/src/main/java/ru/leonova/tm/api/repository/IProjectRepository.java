package ru.leonova.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.exeption.AccessException;
import ru.leonova.tm.exeption.EmptyCollectionException;
import ru.leonova.tm.exeption.EmptyArgumentException;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public interface IProjectRepository {

    @Insert("INSERT INTO app_project (id, name, user_id, statusType, createDate, beginDate, endDate) VALUES (#{projectId}, #{name}, #{userId}, #{status}, #{dateSystem}, #{dateStart}, #{dateEnd})")
    void persist(@NotNull Project project) throws SQLException, ClassNotFoundException;

    @Select("SELECT * FROM app_project")
    @Results(value = {
            @Result(property = "projectId", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "name", column = "name"),
            @Result(property = "status", column = "statusType"),
            @Result(property = "dateSystem", column = "createDate"),
            @Result(property = "dateStart", column = "beginDate"),
            @Result(property = "dateEnd", column = "endDate")})
    Collection<Project> findAll();

    @Select("SELECT * FROM app_project WHERE id = #{id} AND user_id = #{userId}")
    @Results(value = {
            @Result(property = "projectId", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "name", column = "name"),
            @Result(property = "status", column = "statusType"),
            @Result(property = "dateSystem", column = "createDate"),
            @Result(property = "dateStart", column = "beginDate"),
            @Result(property = "dateEnd", column = "endDate")})
    Project findOneById(@NotNull @Param("userId") final String userId, @NotNull @Param("id") final String projectId) throws Exception;

    @Delete("DELETE FROM app_project WHERE id = #{projectId} AND user_id = #{userId}")
    void remove(@NotNull @Param("userId") final String userId, @Param("projectId") final @NotNull String projectId) throws Exception;

    @Delete("DELETE FROM app_project WHERE user_id = #{userId}")
    void removeAllProjectsByUserId(@NotNull @Param("userId") final String userId) throws Exception;

    @Select("SELECT * FROM app_project WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "projectId", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "name", column = "name"),
            @Result(property = "status", column = "statusType"),
            @Result(property = "dateSystem", column = "createDate"),
            @Result(property = "dateStart", column = "beginDate"),
            @Result(property = "dateEnd", column = "endDate")})
    Collection<Project> findAllByUserId(@NotNull @Param("userId") final String userId) throws Exception;

    @Update("UPDATE app_project SET name = #{name} WHERE id = #{id} AND user_id = #{userId}")
    void updateProjectName(@Param("userId") final @NotNull String userId ,@NotNull @Param("id") final String projectId, @NotNull @Param("name") final String name) throws Exception;

    @Select("SELECT FROM app_project WHERE user_id = #{userId} AND id = #{projectId}")
    @Results(value = {
            @Result(property = "projectId", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "name", column = "name"),
            @Result(property = "status", column = "statusType"),
            @Result(property = "dateSystem", column = "createDate"),
            @Result(property = "dateStart", column = "beginDate"),
            @Result(property = "dateEnd", column = "endDate")})
    Project isExist(@NotNull @Param("userId") final String userId, @NotNull @Param("projectId") final String projectId) throws Exception;

    @Select("SELECT FROM app_project WHERE  user_id = #{userId} ORDER BY createDate")
    @Results(value = {
            @Result(property = "projectId", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "name", column = "name"),
            @Result(property = "status", column = "statusType"),
            @Result(property = "dateSystem", column = "createDate"),
            @Result(property = "dateStart", column = "beginDate"),
            @Result(property = "dateEnd", column = "endDate")})
    List<Project>  sortedProjectsBySystemDate(@NotNull String userId) throws Exception;

    @Select("SELECT * FROM app_project WHERE user_id = #{userId} ORDER BY beginDate")
    @Results(value = {
            @Result(property = "projectId", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "name", column = "name"),
            @Result(property = "status", column = "statusType"),
            @Result(property = "dateSystem", column = "createDate"),
            @Result(property = "dateStart", column = "beginDate"),
            @Result(property = "dateEnd", column = "endDate")})
    List<Project> sortedProjectsByStartDate(@Param("userId")@NotNull String userId) throws Exception;

    @Select("SELECT * FROM app_project WHERE  user_id = #{userId} ORDER BY endDate")
    @Results(value = {
            @Result(property = "projectId", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "name", column = "name"),
            @Result(property = "status", column = "statusType"),
            @Result(property = "dateSystem", column = "createDate"),
            @Result(property = "dateStart", column = "beginDate"),
            @Result(property = "dateEnd", column = "endDate")})
    List<Project> sortedProjectsByEndDate(@NotNull @Param("userId") String userId) throws Exception;

    @Select("SELECT * FROM app_project WHERE  user_id = #{userId} ORDER BY statusType")
    @Results(value = {
            @Result(property = "projectId", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "name", column = "name"),
            @Result(property = "status", column = "statusType"),
            @Result(property = "dateSystem", column = "createDate"),
            @Result(property = "dateStart", column = "beginDate"),
            @Result(property = "dateEnd", column = "endDate")})
    List<Project> sortedProjectsByStatus(@NotNull @Param("userId") String userId) throws Exception;

    @Select("SELECT FROM app_project WHERE  user_id = #{userId} AND name=#{projectName}")
    @Results(value = {
            @Result(property = "projectId", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "name", column = "name"),
            @Result(property = "status", column = "statusType"),
            @Result(property = "dateSystem", column = "createDate"),
            @Result(property = "dateStart", column = "beginDate"),
            @Result(property = "dateEnd", column = "endDate")})
    Project searchProjectByName(@NotNull @Param("userId") String userId, @NotNull @Param("projectName") String projectName) throws Exception;

    @Select("SELECT FROM app_project WHERE  user_id = #{userId} AND description=#{description}")
    @Results(value = {
            @Result(property = "projectId", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "name", column = "name"),
            @Result(property = "status", column = "statusType"),
            @Result(property = "dateSystem", column = "createDate"),
            @Result(property = "dateStart", column = "beginDate"),
            @Result(property = "dateEnd", column = "endDate")})
    Project searchProjectByDescription(@NotNull @Param("userId") final String userId, @NotNull @Param("description") final String description) throws Exception;

    @Update("UPDATE app_project SET statusType = #{name} WHERE id = #{id} AND user_id=#{userId}")
    void updateStatusName(@NotNull @Param("id") final String projectId, @NotNull @Param("name") final String name, @NotNull @Param("userId") final String userId) throws SQLException;
}
