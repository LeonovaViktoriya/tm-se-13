package ru.leonova.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.entity.Task;
import ru.leonova.tm.exeption.EmptyArgumentException;
import ru.leonova.tm.exeption.EmptyCollectionException;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public interface ITaskRepository {

    @Insert("INSERT INTO app_task (id, name, user_id, statusType, createDate, beginDate, endDate) VALUES (#{projectId}, #{name}, #{userId}, #{status}, #{dateSystem}, #{dateStart}, #{dateEnd})")
    void persist(@NotNull Task task) throws Exception;

    @Select("SELECT * FROM app_task WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "taskId", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "description", column = "description"),
            @Result(property = "name", column = "name"),
            @Result(property = "status", column = "statusType"),
            @Result(property = "dateSystem", column = "createDate"),
            @Result(property = "dateStart", column = "beginDate"),
            @Result(property = "dateEnd", column = "endDate")})
    Collection<Task> findAllByUserId(@NotNull final @Param("userId") String userId) throws Exception;

    @Update("UPDATE app_task SET name = #{name} WHERE id = #{taskId} AND user_id = #{userId}")
    void updateTaskName(@NotNull final @Param("userId") String userId, @NotNull final @Param("taskId") String taskId, final @Param("name") @NotNull String name) throws SQLException;

    @Select("SELECT * FROM app_task WHERE user_id = #{userId} AND project_id = #{projectId}")
    @Results(value = {
            @Result(property = "taskId", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "description", column = "description"),
            @Result(property = "name", column = "name"),
            @Result(property = "status", column = "statusType"),
            @Result(property = "dateSystem", column = "createDate"),
            @Result(property = "dateStart", column = "beginDate"),
            @Result(property = "dateEnd", column = "endDate")})
    Collection<Task> findAllByProjectId(@NotNull final @Param("userId") String userId, @NotNull final @Param("projectId") String projectId) throws Exception;

    @Delete("DELETE FROM app_task WHERE user_id = #{userId}")
    void removeByIdUser(@NotNull final @Param("userId") String userId) throws Exception;

    @Delete("DELETE FROM app_task WHERE user_id = #{userId} AND id = #{taskId}")
    void removeByIdProject(@NotNull final @Param("userId") String userId, @NotNull final @Param("taskId") String taskId) throws Exception;

    @Select("SELECT * FROM app_task WHERE user_id = #{userId} AND name = #{taskName}")
    @Results(value = {
            @Result(property = "taskId", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "description", column = "description"),
            @Result(property = "name", column = "name"),
            @Result(property = "status", column = "statusType"),
            @Result(property = "dateSystem", column = "createDate"),
            @Result(property = "dateStart", column = "beginDate"),
            @Result(property = "dateEnd", column = "endDate")})
    Task findOneByName(@NotNull final @Param("userId") String userId, @NotNull final @Param("taskName") String taskName) throws Exception;

    @Select("SELECT * FROM app_task WHERE user_id = #{userId} AND id = #{taskId}")
    @Results(value = {
            @Result(property = "taskId", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "description", column = "description"),
            @Result(property = "name", column = "name"),
            @Result(property = "status", column = "statusType"),
            @Result(property = "dateSystem", column = "createDate"),
            @Result(property = "dateStart", column = "beginDate"),
            @Result(property = "dateEnd", column = "endDate")})
    Task findOneById(@NotNull final @Param("userId") String userId, @NotNull final @Param("taskId") String taskId) throws Exception;

    @Select("SELECT * FROM app_task WHERE user_id = #{userId}  ORDER BY endDate")
    @Results(value = {
            @Result(property = "taskId", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "description", column = "description"),
            @Result(property = "name", column = "name"),
            @Result(property = "status", column = "statusType"),
            @Result(property = "dateSystem", column = "createDate"),
            @Result(property = "dateStart", column = "beginDate"),
            @Result(property = "dateEnd", column = "endDate")})
    List<Task> sortByEndDate(@NotNull final @Param("userId") String userId) throws Exception;

    @Select("SELECT * FROM app_task WHERE user_id = #{userId} ORDER BY beginDate")
    @Results(value = {
            @Result(property = "taskId", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "description", column = "description"),
            @Result(property = "name", column = "name"),
            @Result(property = "status", column = "statusType"),
            @Result(property = "dateSystem", column = "createDate"),
            @Result(property = "dateStart", column = "beginDate"),
            @Result(property = "dateEnd", column = "endDate")})
    List<Task> sortByStartDate(@NotNull final @Param("userId") String userId) throws Exception;

    @Select("SELECT * FROM app_task WHERE user_id = #{userId}  ORDER BY createDate")
    @Results(value = {
            @Result(property = "taskId", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "description", column = "description"),
            @Result(property = "name", column = "name"),
            @Result(property = "status", column = "statusType"),
            @Result(property = "dateSystem", column = "createDate"),
            @Result(property = "dateStart", column = "beginDate"),
            @Result(property = "dateEnd", column = "endDate")})
    List<Task> sortBySystemDate(@NotNull final @Param("userId") String userId) throws Exception;

    @Select("SELECT * FROM app_task WHERE user_id = #{userId} ORDER BY statusType")
    @Results(value = {
            @Result(property = "taskId", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "description", column = "description"),
            @Result(property = "name", column = "name"),
            @Result(property = "status", column = "statusType"),
            @Result(property = "dateSystem", column = "createDate"),
            @Result(property = "dateStart", column = "beginDate"),
            @Result(property = "dateEnd", column = "endDate")})
    List<Task> sortByStatus(@NotNull final @Param("userId") String userId) throws Exception;

    @Select("SELECT * FROM app_task")
    @Results(value = {
            @Result(property = "taskId", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "description", column = "description"),
            @Result(property = "name", column = "name"),
            @Result(property = "status", column = "statusType"),
            @Result(property = "dateSystem", column = "createDate"),
            @Result(property = "dateStart", column = "beginDate"),
            @Result(property = "dateEnd", column = "endDate")})
    List<Task> findAll();

    @Delete("DELETE * FROM app_task WHERE user_id = #{userId}")
    void removeAllTasksCollectionByUserId(@NotNull final @Param("userId") String userId) throws SQLException;

    @Delete("DELETE * FROM app_task WHERE user_id = #{userId} AND  project_id = #{projectId}")
    void removeAllTasksCollectionByProjectId(@NotNull final @Param("userId") String userId, final @Param("projectId") @NotNull String projectId) throws SQLException;
}
