package ru.leonova.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.service.IProjectService;
import ru.leonova.tm.api.service.ISessionService;
import ru.leonova.tm.api.service.ITaskService;
import ru.leonova.tm.api.service.IUserService;

public interface ServiceLocator {

    @NotNull IProjectService getProjectService();

    @NotNull IUserService getUserService();

    @NotNull ITaskService getTaskService();

//    IDomainEndpoint getDomainService();

    @NotNull ISessionService getSessionService();
}
