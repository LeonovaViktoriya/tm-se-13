package ru.leonova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.entity.Project;

import java.util.Collection;
import java.util.List;

public interface IProjectService {

    void load(@NotNull List<Project> list) throws Exception;

    List<Project> save();

    void merge(@NotNull Project project) throws Exception;

    void create(@NotNull String userId, @NotNull Project project) throws Exception;

    boolean isExist(@NotNull String userId, @NotNull String projectId) throws Exception;

    void updateNameProject(@NotNull String userId, @NotNull String projectId, @NotNull String name) throws Exception;

    Collection<Project> findAllByUserId(@NotNull String userId) throws Exception;

    void deleteProject(@NotNull String userId, @NotNull String projectId) throws Exception;

    void deleteAllProject(String userId) throws Exception;

    List<Project> sortProjectsBySystemDate(@NotNull String userId) throws Exception;

    List<Project> sortProjectsByStartDate(@NotNull String userId) throws Exception;

    List<Project> sortProjectsByEndDate(@NotNull String userId) throws Exception;

    List<Project> sortProjectsByStatus(@NotNull String userId) throws Exception;

    Project searchProjectByName(@NotNull String userId, @NotNull String projectName) throws Exception;

    Project searchProjectByDescription(@NotNull String userId, @NotNull String description) throws Exception;

    void updateStatusProject(@NotNull String userId, @NotNull String projectId, @NotNull String name) throws Exception;
}
