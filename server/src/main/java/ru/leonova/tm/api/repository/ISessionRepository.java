package ru.leonova.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.entity.Session;
import ru.leonova.tm.enumerated.RoleType;
import ru.leonova.tm.exeption.AccessException;
import ru.leonova.tm.exeption.EmptyArgumentException;

import java.sql.SQLException;
import java.util.Collection;

public interface ISessionRepository {

    @Insert("INSERT INTO app_session (id, signature, user_id, timestamp) VALUES (#{sessionId}, #{signature}, #{userId}, #{timestamp})")
    void persist(@NotNull final Session session) throws Exception;

    @Update("UPDATE app_session SET (id, signature, user_id, timestamp) VALUES (#{sessionId}, #{signature}, #{userId}, #{timestamp}")
    void update(String sessionId, RoleType roleType, String userId, String signature, Long timestamp) throws SQLException;

    @Select("SELECT * FROM app_session WHERE id = #{sessionId}")
    @Results(value = {
            @Result(property = "sessionId", column = "id"),
            @Result(property = "signature", column = "signature"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "timestamp", column = "timestamp")})
    Session findSessionById(@NotNull @Param("sessionId")String sessionId) throws Exception;

    @Select("SELECT * FROM app_session WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "sessionId", column = "id"),
            @Result(property = "signature", column = "signature"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "timestamp", column = "timestamp")})
    Session findSessionByUserId(@NotNull @Param("userId") String userId) throws Exception;

    @Delete("DELETE * FROM app_task WHERE id = #{sessionId}")
    void remove(@NotNull @Param("userId")String sessionId) throws Exception;

}
