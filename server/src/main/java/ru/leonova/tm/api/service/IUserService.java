package ru.leonova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.entity.Session;
import ru.leonova.tm.entity.User;
import ru.leonova.tm.exeption.EmptyArgumentException;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public interface IUserService {


    void merge(@NotNull User user) throws Exception;

    void load(@NotNull List<User> list) throws Exception;

    void updateLogin(@NotNull User user, @NotNull String login) throws Exception;

    User authorizationUser(@NotNull String login, @NotNull String password) throws Exception;

    User getById(@NotNull String userId) throws Exception;

    List<User> save() throws SQLException;

    void removeUser(@NotNull String userId) throws Exception;

    Collection<User> getCollection() throws Exception;

    void adminRegistration(@NotNull String admin, @NotNull String admin1) throws Exception;

    String md5Apache(@NotNull String password) throws Exception;

    void addAll(@NotNull List<User> users) throws Exception;

    void updatePassword(@NotNull User user, @NotNull String password) throws Exception;
}
