package ru.leonova.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.enumerated.RoleType;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class Session implements Cloneable, Serializable {

    @NotNull
    private String sessionId;

    @NotNull
    private String userId;

    @NotNull
    private RoleType roleType;

    @NotNull
    private Long timestamp;

    private String signature;

    @Override
    public Session clone() throws CloneNotSupportedException {
        return (Session) super.clone();
    }
}
