package ru.leonova.tm.service;

import lombok.Cleanup;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.repository.IProjectRepository;
import ru.leonova.tm.api.service.IProjectService;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.utils.SqlSessionFactoryClass;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public final class ProjectService extends AbstractService<Project> implements IProjectService {

    @Override
    public void load(@NotNull final List<Project> list) throws Exception {
        if (list.isEmpty()) throw new Exception();
        @NotNull final SqlSessionFactory sqlSessionFactory = SqlSessionFactoryClass.getSqlSessionFactory();
        @Cleanup @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        list.forEach(project -> {
            try {
                projectRepository.persist(project);
            } catch (SQLException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public List<Project> save() {
        @NotNull final SqlSessionFactory sqlSessionFactory = SqlSessionFactoryClass.getSqlSessionFactory();
        @Cleanup @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        return (List<Project>) projectRepository.findAll();
    }

    @Override
    public void merge(@NotNull final Project project) throws Exception {
        @NotNull final SqlSessionFactory sqlSessionFactory = SqlSessionFactoryClass.getSqlSessionFactory();
        @Cleanup @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        if (project.getProjectId() == null || project.getProjectId().isEmpty()) throw new Exception();
        @NotNull final Project projectFind = projectRepository.findOneById(project.getUserId(),project.getProjectId());
        if(projectFind!=null) projectRepository.updateProjectName(project.getUserId(), projectFind.getProjectId(), project.getName());
        else projectRepository.persist(project);
    }

    @Override
    public void create(@NotNull final String userId, @NotNull final Project project) throws Exception {
        @NotNull final String projectId = project.getProjectId();
        if (userId.isEmpty() || projectId.isEmpty()) throw new Exception("Id user or Id project is empty!");
        if (!project.getUserId().equals(userId)) throw new Exception("Access denied!");
        @NotNull final SqlSessionFactory sqlSessionFactory = SqlSessionFactoryClass.getSqlSessionFactory();
        @Cleanup @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            if (isExist(userId,projectId)) merge(project);
            else projectRepository.persist(project);
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
            e.printStackTrace();
        }
    }

    @Override
    public boolean isExist(@NotNull final String userId, @NotNull final String projectId) throws Exception {
        if (projectId.isEmpty()) throw new Exception();
        @NotNull final SqlSessionFactory sqlSessionFactory = SqlSessionFactoryClass.getSqlSessionFactory();
        @Cleanup @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        Project project = projectRepository.isExist(userId, projectId);
        return project!=null;
    }

    @Override
    public void updateNameProject(@NotNull final String userId, @NotNull final String projectId, @NotNull final String name) throws Exception {
        System.out.println(projectId);
        if (userId.isEmpty() || projectId.isEmpty() || name.isEmpty()) throw new Exception();
        @NotNull final SqlSessionFactory sqlSessionFactory = SqlSessionFactoryClass.getSqlSessionFactory();
        @Cleanup @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        @NotNull final Project project = projectRepository.findOneById(userId, projectId);
        if (project == null || !project.getUserId().equals(userId)) throw new Exception("Access denied!");
        try {
            projectRepository.updateProjectName(userId, projectId, name);
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
            e.printStackTrace();
        }
    }

    @Override
    public Collection<Project> findAllByUserId(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new Exception();
        @NotNull final SqlSessionFactory sqlSessionFactory = SqlSessionFactoryClass.getSqlSessionFactory();
        @Cleanup @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            Collection<Project> projectCollection = projectRepository.findAllByUserId(userId);
            sqlSession.commit();
            return projectCollection;
        } catch (SQLException e) {
            sqlSession.rollback();
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void deleteProject(@NotNull final String userId, @NotNull final String projectId) throws Exception {
        if (projectId.isEmpty() || userId.isEmpty()) throw new Exception();
        @NotNull final SqlSessionFactory sqlSessionFactory = SqlSessionFactoryClass.getSqlSessionFactory();
        @Cleanup @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        @NotNull final Project project = projectRepository.findOneById(userId, projectId);
        if (project != null && project.getUserId().equals(userId)) {
            try {
                projectRepository.remove(userId, project.getProjectId());
                sqlSession.commit();
            } catch (SQLException e) {
                sqlSession.rollback();
                e.printStackTrace();
            }
        }
    }

    @Override
    public void deleteAllProject(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new Exception();
        @NotNull final SqlSessionFactory sqlSessionFactory = SqlSessionFactoryClass.getSqlSessionFactory();
        @Cleanup @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            projectRepository.removeAllProjectsByUserId(userId);
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
            e.printStackTrace();
        }
    }

    @Override
    public List<Project> sortProjectsBySystemDate(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new Exception();
        @NotNull final SqlSessionFactory sqlSessionFactory = SqlSessionFactoryClass.getSqlSessionFactory();
        @Cleanup @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        return projectRepository.sortedProjectsBySystemDate(userId);
    }

    @Override
    public List<Project> sortProjectsByStartDate(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new Exception();
        @NotNull final SqlSessionFactory sqlSessionFactory = SqlSessionFactoryClass.getSqlSessionFactory();
        @Cleanup @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        return projectRepository.sortedProjectsByStartDate(userId);
    }

    @Override
    public List<Project> sortProjectsByEndDate(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new Exception();
        @NotNull final SqlSessionFactory sqlSessionFactory = SqlSessionFactoryClass.getSqlSessionFactory();
        @Cleanup @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        return projectRepository.sortedProjectsByEndDate(userId);
    }

    @Override
    public List<Project> sortProjectsByStatus(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new Exception();
        @NotNull final SqlSessionFactory sqlSessionFactory = SqlSessionFactoryClass.getSqlSessionFactory();
        @Cleanup @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        return projectRepository.sortedProjectsByStatus(userId);
    }

    @Override
    public Project searchProjectByName(@NotNull final String userId, @NotNull final String projectName) throws Exception {
        if (userId.isEmpty() || projectName.isEmpty()) throw new Exception();
        @NotNull final SqlSessionFactory sqlSessionFactory = SqlSessionFactoryClass.getSqlSessionFactory();
        @Cleanup @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            Project project = projectRepository.searchProjectByName(userId, projectName);
            sqlSession.commit();
            return project;
        } catch (SQLException e) {
            sqlSession.rollback();
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Project searchProjectByDescription(@NotNull final String userId, @NotNull final String description) throws Exception {
        if (userId.isEmpty() || description.isEmpty()) throw new Exception();
        @NotNull final SqlSessionFactory sqlSessionFactory = SqlSessionFactoryClass.getSqlSessionFactory();
        @Cleanup @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            Project project = projectRepository.searchProjectByDescription(description, userId);
            sqlSession.commit();
            return project;
        } catch (SQLException e) {
            sqlSession.rollback();
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void updateStatusProject(@NotNull String userId, @NotNull String projectId, @NotNull String name) throws Exception {
        if (userId.isEmpty() || projectId.isEmpty() || name.isEmpty()) throw new Exception();
        @NotNull final SqlSessionFactory sqlSessionFactory = SqlSessionFactoryClass.getSqlSessionFactory();
        @Cleanup @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            projectRepository.updateStatusName(projectId, name, userId);
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
            e.printStackTrace();
        }
    }
}
