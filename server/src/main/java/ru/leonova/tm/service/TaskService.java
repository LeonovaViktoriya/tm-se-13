package ru.leonova.tm.service;

import lombok.Cleanup;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.repository.IProjectRepository;
import ru.leonova.tm.api.repository.ITaskRepository;
import ru.leonova.tm.api.service.ITaskService;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.entity.Task;
import ru.leonova.tm.utils.SqlSessionFactoryClass;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public final class TaskService extends AbstractService<Task> implements ITaskService {

    @Override
    public void create(@NotNull final String userId, @NotNull final Task task) throws Exception {
        if (userId.isEmpty()) throw new Exception();
        @NotNull final SqlSessionFactory sqlSessionFactory = SqlSessionFactoryClass.getSqlSessionFactory();
        @Cleanup @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        if (!task.getUserId().equals(userId)) throw new Exception();
        try {
            if(isExist(userId, task.getTaskId())) taskRepository.persist(task);
            else taskRepository.persist(task);
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
            e.printStackTrace();
        }
    }
    @Override
    public boolean isExist(@NotNull final String userId, @NotNull final String taskId) throws Exception {
        if (taskId.isEmpty()) throw new Exception();
        @NotNull final SqlSessionFactory sqlSessionFactory = SqlSessionFactoryClass.getSqlSessionFactory();
        @Cleanup @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        Task task = taskRepository.findOneById(userId, taskId);
        return task!=null;
    }

    @Override
    public void load(@NotNull List<Task> list) throws Exception {
        if (list.isEmpty()) throw new Exception();
        @NotNull final SqlSessionFactory sqlSessionFactory = SqlSessionFactoryClass.getSqlSessionFactory();
        @Cleanup @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        list.forEach(project -> {
            try {
                taskRepository.persist(project);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public List<Task> save() {
        @NotNull final SqlSessionFactory sqlSessionFactory = SqlSessionFactoryClass.getSqlSessionFactory();
        @Cleanup @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        return taskRepository.findAll();
    }

    @Override
    public void updateTaskName(@NotNull String userId, @NotNull final String taskId, @NotNull final String taskName) throws Exception {
        if (taskName.isEmpty() || taskId.isEmpty() || taskId.equals(userId)) throw new Exception();
        @NotNull final SqlSessionFactory sqlSessionFactory = SqlSessionFactoryClass.getSqlSessionFactory();
        @Cleanup @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            taskRepository.updateTaskName(userId, taskId, taskName);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        }
    }

    @Override
    public void deleteTaskByIdProject(@NotNull final String userId, @NotNull final String projectId) throws Exception {
        if (projectId.isEmpty() || userId.isEmpty()) throw new Exception();
        @NotNull final SqlSessionFactory sqlSessionFactory = SqlSessionFactoryClass.getSqlSessionFactory();
        @Cleanup @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            taskRepository.removeByIdProject(userId, projectId);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        }
    }

    @Override
    public Collection<Task> findAllByUserId(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new Exception();
        @NotNull final SqlSessionFactory sqlSessionFactory = SqlSessionFactoryClass.getSqlSessionFactory();
        @Cleanup @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            @NotNull final Collection<Task> taskCollection = taskRepository.findAllByUserId(userId);
            sqlSession.commit();
            return taskCollection;
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Collection<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) throws Exception {
        if (userId.isEmpty() || projectId.isEmpty()) throw new Exception();
        @NotNull final SqlSessionFactory sqlSessionFactory = SqlSessionFactoryClass.getSqlSessionFactory();
        @Cleanup @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            @NotNull final Collection<Task> taskCollection = taskRepository.findAllByProjectId(userId, projectId);
            sqlSession.commit();
            return taskCollection;
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
            return null;
        }
    }


    @Override
    public void deleteTaskByIdUser(@NotNull final String userId, @NotNull final String taskId) throws Exception {
        if (taskId.isEmpty() || userId.isEmpty()) throw new Exception();
        @NotNull final SqlSessionFactory sqlSessionFactory = SqlSessionFactoryClass.getSqlSessionFactory();
        @Cleanup @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            taskRepository.removeByIdUser(userId);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        }
    }


    @Override
    public void deleteAllTaskByUserId(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new Exception();
        @NotNull final SqlSessionFactory sqlSessionFactory = SqlSessionFactoryClass.getSqlSessionFactory();
        @Cleanup @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            taskRepository.removeAllTasksCollectionByUserId(userId);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        }
    }

    @Override
    public void merge(@NotNull final Task task) throws Exception {
        @NotNull final SqlSessionFactory sqlSessionFactory = SqlSessionFactoryClass.getSqlSessionFactory();
        @Cleanup @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        if (task.getProjectId() == null || task.getProjectId().isEmpty()) throw new Exception();
        @NotNull final Task task1 = taskRepository.findOneByName(task.getUserId(),task.getName());
        if(task1!=null) taskRepository.updateTaskName(task.getUserId(), task1.getProjectId(), task.getName());
        else taskRepository.persist(task);
    }

    @Override
    public void deleteAllTaskByProjectId(@NotNull String userId, @NotNull final String projectId) throws Exception {
        if (userId.isEmpty() || projectId.isEmpty()) throw new Exception();
        @NotNull final SqlSessionFactory sqlSessionFactory = SqlSessionFactoryClass.getSqlSessionFactory();
        @Cleanup @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            taskRepository.removeAllTasksCollectionByProjectId(userId, projectId);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        }
    }

    @Override
    public Task findTaskByName(@NotNull final String userId, @NotNull final String taskName) throws Exception {
        if (userId.isEmpty() || taskName.isEmpty()) throw new Exception();
        @NotNull final SqlSessionFactory sqlSessionFactory = SqlSessionFactoryClass.getSqlSessionFactory();
        @Cleanup @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            Task task = taskRepository.findOneByName(userId, taskName);
            sqlSession.commit();
            return task;
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<Task> sortTasksByEndDate(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new Exception();
        @NotNull final SqlSessionFactory sqlSessionFactory = SqlSessionFactoryClass.getSqlSessionFactory();
        @Cleanup @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            @NotNull final List<Task> taskCollection = taskRepository.sortByEndDate(userId);
            sqlSession.commit();
            return taskCollection;
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<Task> sortTasksByStartDate(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new Exception();
        @NotNull final SqlSessionFactory sqlSessionFactory = SqlSessionFactoryClass.getSqlSessionFactory();
        @Cleanup @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            @NotNull final List<Task> taskCollection = taskRepository.sortByStartDate(userId);
            sqlSession.commit();
            return taskCollection;
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<Task> sortTasksBySystemDate(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new Exception();
        @NotNull final SqlSessionFactory sqlSessionFactory = SqlSessionFactoryClass.getSqlSessionFactory();
        @Cleanup @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            @NotNull final List<Task> taskCollection = taskRepository.sortBySystemDate(userId);
            sqlSession.commit();
            return taskCollection;
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<Task> sortTasksByStatus(String userId) throws Exception {
        if (userId.isEmpty()) throw new Exception();
        @NotNull final SqlSessionFactory sqlSessionFactory = SqlSessionFactoryClass.getSqlSessionFactory();
        @Cleanup @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            @NotNull final List<Task> taskCollection =  taskRepository.sortByStatus(userId);
            sqlSession.commit();
            return taskCollection;
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
            return null;
        }
    }

}
