package ru.leonova.tm.service;

import lombok.Cleanup;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.repository.IProjectRepository;
import ru.leonova.tm.api.repository.IUserRepository;
import ru.leonova.tm.api.service.IUserService;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.entity.User;
import ru.leonova.tm.enumerated.RoleType;
import ru.leonova.tm.utils.SqlSessionFactoryClass;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public final class UserService extends AbstractService<User> implements IUserService {

    @Override
    public void merge(@NotNull final User user) throws Exception {
        @NotNull final SqlSessionFactory sqlSessionFactory = SqlSessionFactoryClass.getSqlSessionFactory();
        @Cleanup @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        @NotNull final User userFind = userRepository.findOne(user.getUserId());
        try {
            if (userFind != null) {
                userRepository.updateLogin(user.getUserId(), user.getLogin());
                userRepository.updatePassword(user.getUserId(), user.getPassword());
            } else userRepository.persist(user);
        } catch (SQLException e) {
            sqlSession.rollback();
            e.printStackTrace();
        }
    }

    @Override
    public void load(@NotNull final List<User> list) throws Exception {
        if (list.isEmpty()) throw new Exception();
        @NotNull final SqlSessionFactory sqlSessionFactory = SqlSessionFactoryClass.getSqlSessionFactory();
        @Cleanup @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        for (@NotNull final User user : list) {
            try {
                userRepository.persist(user);
                sqlSession.commit();
            } catch (SQLException e) {
                sqlSession.rollback();
                e.printStackTrace();
            }
        }
    }

    @Override
    public List<User> save() throws SQLException {
        @NotNull final SqlSessionFactory sqlSessionFactory = SqlSessionFactoryClass.getSqlSessionFactory();
        @Cleanup @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        return (List<User>) userRepository.findAll();
    }

    @Override
    public void removeUser(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new Exception("User not found");
        @NotNull final SqlSessionFactory sqlSessionFactory = SqlSessionFactoryClass.getSqlSessionFactory();
        @Cleanup @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            userRepository.remove(userId);
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
            e.printStackTrace();
        }
    }

    @Override
    public Collection<User> getCollection() {
        @NotNull final SqlSessionFactory sqlSessionFactory = SqlSessionFactoryClass.getSqlSessionFactory();
        @Cleanup @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            @NotNull final Collection<User> users = userRepository.findAll();
            sqlSession.commit();
            return users;
        } catch (SQLException e) {
            sqlSession.rollback();
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void updateLogin(@NotNull final User user, @NotNull final String login) throws Exception {
        if (login.isEmpty() || user.getLogin().isEmpty()) throw new Exception("login is empty!");
        @NotNull final SqlSessionFactory sqlSessionFactory = SqlSessionFactoryClass.getSqlSessionFactory();
        @Cleanup @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            userRepository.updateLogin(user.getUserId(), login);
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
            e.printStackTrace();
        }
    }

    @Override
    public User authorizationUser(@NotNull final String login, @NotNull final String password) throws Exception {
        if (login.isEmpty() || password.isEmpty()) throw new Exception("login or password is empty!");
        @NotNull final SqlSessionFactory sqlSessionFactory = SqlSessionFactoryClass.getSqlSessionFactory();
        @Cleanup @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            @NotNull final User user = userRepository.findByLoginAndPassword(login, password);
            user.setRoleType(RoleType.USER.getRole());
            sqlSession.commit();
            return user;
        } catch (SQLException e) {
            sqlSession.rollback();
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public User getById(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new Exception("Id user is empty!");
        @NotNull final SqlSessionFactory sqlSessionFactory = SqlSessionFactoryClass.getSqlSessionFactory();
        @Cleanup @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            @NotNull final User user = userRepository.findOne(userId);
            sqlSession.commit();
            return user;
        } catch (SQLException e) {
            sqlSession.rollback();
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public String md5Apache(@NotNull final String password) throws Exception {
        if (password.isEmpty()) throw new Exception("password is empty!");
        return DigestUtils.md5Hex(password);
    }

    @Override
    public void addAll(@NotNull List<User> users) {
        @NotNull final SqlSessionFactory sqlSessionFactory = SqlSessionFactoryClass.getSqlSessionFactory();
        @Cleanup @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            users.addAll(userRepository.findAll());
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
            e.printStackTrace();
        }
    }

    @Override
    public void updatePassword(@NotNull User user, @NotNull String password) throws Exception {
        if (password.isEmpty()) throw new Exception("password is empty!");
        @NotNull final SqlSessionFactory sqlSessionFactory = SqlSessionFactoryClass.getSqlSessionFactory();
        @Cleanup @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            userRepository.updatePassword(user.getUserId(), password);
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
            e.printStackTrace();
        }
    }

    @Override
    public void adminRegistration(@NotNull final String login, @NotNull String password) throws Exception {
        if (login.isEmpty() || password.isEmpty()) throw new Exception("login or password is empty!");
        @NotNull final SqlSessionFactory sqlSessionFactory = SqlSessionFactoryClass.getSqlSessionFactory();
        @Cleanup @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            @NotNull final User admin = new User("admin", md5Apache("admin"));
            admin.setRoleType(RoleType.ADMIN.getRole());
            userRepository.persist(admin);
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
            e.printStackTrace();
        }
    }

}
