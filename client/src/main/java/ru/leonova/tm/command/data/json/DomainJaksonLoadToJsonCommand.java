package ru.leonova.tm.command.data.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.endpoint.Domain;
import ru.leonova.tm.api.endpoint.Session;
import ru.leonova.tm.command.AbstractCommand;

import java.io.File;

public class DomainJaksonLoadToJsonCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "jackson-loadUserList-json";
    }

    @Override
    public String getDescription() {
        return "Load data to json file via jackson";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        final Session session = serviceLocator.getCurrentSession();
        if(session==null) throw new Exception("You are have not session");
        @NotNull final Domain domain = new Domain();
        serviceLocator.getDomainEndpoint().save(session, domain);
        @NotNull final File file = new File("./data/jackson.json");
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final Domain domainResult = mapper.readValue(file, Domain.class);
        serviceLocator.getDomainEndpoint().load(session, domainResult);
        System.out.println("Data loadUserList from " + file.getPath());
        System.out.println(domainResult.getProjects());
        System.out.println(domainResult.getTasks());
        System.out.println(domainResult.getUsers());
    }
}
