package ru.leonova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.endpoint.Session;
import ru.leonova.tm.api.endpoint.Task;
import ru.leonova.tm.command.AbstractCommand;

import java.util.Collection;

public final class TaskDeleteCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "del-t";
    }

    @Override
    public String getDescription() {
        return "Delete task";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        final Session session = serviceLocator.getCurrentSession();
        if(session==null) throw new Exception("You are have not session");
        System.out.println("[DELETE TASK BY ID]\n[Task list:]");
        @NotNull final Collection<Task> taskCollection = serviceLocator.getTaskEndpoint().findAllTasksByUserId(session);
        int i = 0;
        for (Task task : taskCollection) {
            i++;
            System.out.println(i + ". PROJECT ID: " + task.getProjectId() + ",TASK ID: " + task.getTaskId() + ", TASK NAME: " + task.getName());
        }
        System.out.println("Enter task id:");
        @NotNull final String taskId = getScanner().nextLine();
        if (taskId == null || taskId.isEmpty()) return;
        serviceLocator.getTaskEndpoint().deleteTask(session, taskId);
        System.out.println("Task deleted");
    }
}
