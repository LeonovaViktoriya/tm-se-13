package ru.leonova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.endpoint.Session;
import ru.leonova.tm.command.AbstractCommand;

public final class ProjectDeleteListCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "del-all-p";
    }

    @Override
    public String getDescription() {
        return "Delete all projects this user";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("["+getDescription().toUpperCase()+"]");
        final Session session = serviceLocator.getCurrentSession();
        if(session==null) throw new Exception("You are have not session");
        serviceLocator.getProjectEndpoint().deleteAllProject(session);
//        serviceLocator.getTaskEndpoint().deleteAllTaskByUserId(session);
        System.out.println("Deleted");
    }
}
