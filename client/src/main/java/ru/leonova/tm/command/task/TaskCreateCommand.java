package ru.leonova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.endpoint.Project;
import ru.leonova.tm.api.endpoint.Session;
import ru.leonova.tm.api.endpoint.Task;
import ru.leonova.tm.command.AbstractCommand;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Scanner;

public final class TaskCreateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "create-t";
    }

    @Override
    public String getDescription() {
        return "Create task";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CREATE TASK]\nList projects:");
        final Session session = serviceLocator.getCurrentSession();
        if(session==null) throw new Exception("You are have not session");
        Collection<Project> projectCollection = serviceLocator.getProjectEndpoint().findAllProjectsByUserId(session);
        int i = 0;
        for (Project project : projectCollection) {
            i++;
            System.out.println(i + ". ID PROJECT: " + project.getProjectId() + ", NAME: " + project.getName());
        }
        System.out.println("\nSELECT ID PROJECT: ");
        String projectId = getScanner().nextLine();;
        System.out.println("Enter name task: ");
        Scanner input = new Scanner(System.in);
        String name = input.nextLine();
        Task task = new Task();
        task.setName(name);
        task.setProjectId(projectId);
        task.setUserId(session.getUserId());
        task.setStatus("planned");
        System.out.println("Date start task:");
        String date1 = getScanner().nextLine();
        System.out.println("Date end task:");
        String date2 = getScanner().nextLine();
        @NotNull final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date dateStart = format.parse(date1);
        Date dateEnd = format.parse(date2);
        Date dateSystem = new Date();
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(dateStart);
        XMLGregorianCalendar xmlGregCalDateStart = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
        cal.setTime(dateEnd);
        XMLGregorianCalendar xmlGregCalDateEnd = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
        cal.setTime(dateSystem);
        XMLGregorianCalendar xmlGregCalDateSystem =  DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
        serviceLocator.getTaskEndpoint().createTask(session,task, xmlGregCalDateStart, xmlGregCalDateEnd, xmlGregCalDateSystem);
        System.out.println("Task created");
    }
}
