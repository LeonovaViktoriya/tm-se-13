package ru.leonova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.endpoint.Project;
import ru.leonova.tm.api.endpoint.Session;
import ru.leonova.tm.command.AbstractCommand;

import java.text.SimpleDateFormat;
import java.util.List;

public class ProjectsSortByStatus extends AbstractCommand {
    @Override
    public String getName() {
        return "p-sort-by-status";
    }

    @Override
    public String getDescription() {
        return "Sorted projects by status";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        final Session session = serviceLocator.getCurrentSession();
        if(session==null) throw new Exception("You are have not session");
        @NotNull final List<Project> projects = serviceLocator.getProjectEndpoint().sortProjectsByStatus(session);
        System.out.println("\nAfter Sorting by status:");
        @NotNull final SimpleDateFormat format = new SimpleDateFormat();
        for (@NotNull final Project project : projects) {
            System.out.println(project.getName() + " " + format.format(project.getDateSystem()));
        }
    }
}
