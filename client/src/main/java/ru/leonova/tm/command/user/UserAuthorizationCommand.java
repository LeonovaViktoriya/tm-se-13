package ru.leonova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.endpoint.Session;
import ru.leonova.tm.command.AbstractCommand;
import ru.leonova.tm.exeption.AccessException;
import ru.leonova.tm.utils.PasswordHashUtil;

public final class UserAuthorizationCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "auth";
    }

    @Override
    public String getDescription() {
        return "Authorization user";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("["+getDescription().toUpperCase()+"]\nEnter user login:");
        @NotNull final String login = getScanner().nextLine();
        System.out.println("Enter user password:");
        @NotNull final String password = getScanner().nextLine();
        @NotNull final Session session = serviceLocator.getUserEndpoint().authorizationUserAndOpenSession(login, PasswordHashUtil.md5(password));
        if(serviceLocator.getSessionEndpoint().valid(session)){
            serviceLocator.setCurrentSession(session);
            System.out.println("Authorization was successful");
        }else {
            System.out.println("This user does not registered");
        }
    }

}
