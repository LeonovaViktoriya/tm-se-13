package ru.leonova.tm;

import ru.leonova.tm.bootstrap.Bootstrap;
//import ru.leonova.tm.exeption.EmptyArgumentException;

/**
 * Hello world!
 *
 */
public class App {

    public static void main( String[] args ) throws Exception {
        System.out.println("It's client!");
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }
}
