package ru.leonova.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.leonova.tm.api.endpoint.*;
import ru.leonova.tm.command.AbstractCommand;


import java.util.List;

public interface ServiceLocator {

    Session getCurrentSession();

    void setCurrentSession(Session session);

    @NotNull List<AbstractCommand> getCommands();

    @NotNull IProjectEndpoint getProjectEndpoint();

    @NotNull IUserEndpoint getUserEndpoint();

    @NotNull ITaskEndpoint getTaskEndpoint();

    @NotNull ISessionEndpoint getSessionEndpoint();

    @NotNull IDomainEndpoint getDomainEndpoint();
}
